import threading
import queue
import multiprocessing as mp
import warnings
import sys

NPROCS = mp.cpu_count()


def set_processing(pwish: str = None, force_wish: bool = False):
    proc_choices = ("process", "proc", "subproc", "subprocess")
    if sys.platform.startswith('win32') and not force_wish:
        ptype, qtype = threading.Thread, queue.Queue
        if pwish.lower() in proc_choices:
            warnings.warn(
                f"Cannot use multiprocesing on this platform ({sys.platform}) "
                f"for this task. Using threads instead."
            )
    else:
        if not pwish:
            pwish = "process"
        if pwish.lower() in proc_choices:
            ptype, qtype = mp.Process, mp.Queue
        else:
            ptype, qtype = threading.Thread, queue.Queue
    return ptype, qtype
