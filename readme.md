
### Measure the Amount of Melt Visible in One or More Frames of a Video

Helps with the work flow to identify hot melt (or anything else when providing
an appropriate detector function) in video records. Measures area of melt
visible in one or more frames of a video.

This originates in the *analysis of explosive magma-water interaction experiments*.
For example published here:
[`10.1029/2018jb015682`](https://doi.org/10.1029/2018jb015682).

To convert the example video to an image sequence the `ffmpeg` video
decoder/converter is necessary. The code needs a Python distribution. Therefore,
before reading the example, have a look at the [install and setup notes](doc/setup.rst).

Once set up the [example notebook](doc/example.ipynb) provides a walk through
this is meant to be used.


#### Analysis of Explosive Magma-Water Interaction Experiments

Quantifying what happens when molten rock comes into direct contact with liquid
water is difficult, even if it can be observed directly in an experiment.
It is hard to install sensors anywhereclose to the melt at temperatures larger
than 1000°C. Therefore, this somewhat rough method was developed which analyzes
the appearance of melt on video records.

The code does nothing else than either summing up the luminance (brightness) of
each pixel in a video frame, or summing up pixels identified to show melt. In
the latter case identification is often achieved by color analysis of each
pixel. Per-video user supplied identification functions seem to be the best way
to do this. Spacial resolution and error correction then allow to compute the
absolude (projected) area of melt visible in a given video frame.
