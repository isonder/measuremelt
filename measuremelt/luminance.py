"""All luminance related functions"""
import numpy as np
from numpy import ndarray
import scipy.signal as sig

from . import NPROCS, set_processing

ProcOrThread, Queue = set_processing()


def cumul_bright(frame, select=None):
    """Computes the cumulative, relative luminance of an image.

    Parameters
    ----------
    frame : ndarray
        Image to compute the luminance from.
    select : tuple, optional
        ((start0, end0), (start1, end1)). Optional, to select subset of `frame`.

    Returns
    -------
        float
    Brightness of frame.
    """
    if select is None:
        start0, start1 = 0, 0
        end0, end1 = frame.shape
    else:
        ((start0, end0), (start1, end1)) = select
    return frame[start0:end0, start1:end1].sum()


def luminance(frame, fov, ref, noise, select=None):
    """Computes the luminance from a given video frame.

    Parameters
    ----------
    frame : 2D ndarray
        Image frame to compute the luminance from.
    fov : float
        Field of view (square meters).
    ref : float
        Cumulative reference brightness.
    noise : float
        Cumulative brightness of background noise.
    select : tuple, optional
        Selection marking the subset of `frame` to compute the luminance from.

    Returns
    -------
        float
    luminance of `frame`.
    """
    return fov * (cumul_bright(frame, select) - noise) / (ref - noise)


def _cbright_chunk(chunk, select, start, end, que):
    """**Do not call this directly.**
     Computes the cumulative brightness for a part of an image sequence. This
     method should is called multiple times from `cumul_bright_sequence()` in
     separate processes.

    Parameters
    ----------
    chunk : Slicerator
        Complete or partial image  sequence.
    select : tuple
        Selection marking the subset of `frame` to compute the luminance from.
    start : int
        Start position in sequence from which `chunk` was selected.
    end : int
        End position in sequence from which `chunk` was selected.
    que : _mp.Queue
        The queue object that manages the multiple processes.
    """
    act = np.empty(len(chunk), dtype=float)
    for i, frame in enumerate(chunk):
        act[i] = cumul_bright(frame, select)
    que.put((act, start, end))


def cumul_bright_sequence(seq, select=None, processes=NPROCS):
    """Compute the cumulative brightness of each frame in `seq` using the
    `luminance()` function. Arguments other than `seq` and `processes are
    passed unmodified to `cumul_brightness()`.

    Parameters
    ----------
    seq : Slicerator
        Image sequence to compute the luminance from.
    select : tuple
        ((start0, end0), (start1, end1)). Optional, to select subset of a frame
         in `seq`.
    processes : int
        Number of system processes to use. Default is number of system CPUs.

    Returns
    -------
        ndarray
    Brightness array B(t)
    """
    itms_per_proc = len(seq) // processes
    que = Queue()
    start, end, procs, idx = 0, -1, [], []
    for k in range(processes):
        end = start + itms_per_proc
        p = ProcOrThread(
            target=_cbright_chunk,
            args=(seq[start:end], select, start, end, que))
        procs.append(p)
        start = end
        p.start()
    p = ProcOrThread(
        target=_cbright_chunk,
        args=(seq[start:], select, start, len(seq), que))
    procs.append(p)
    p.start()
    act = np.empty(len(seq), dtype=float)
    for _ in range(len(procs)):
        chunk, start, end = que.get()
        try:
            act[start:end] = chunk
        except ValueError:
            print("chunk:", chunk)
            print("start:", start, "end:", end)
            raise
    for p in procs:
        p.join()
    return act


def average_cbright(chunk, select=None, uncert=False, nprocs=NPROCS):
    """Convenience method to compute the average cumulative brightness of
    given image sequence selection. Useful to determine the noise level.

    Parameters
    ----------
    chunk : Slicerator
        Image sequence/chunk.
    select : tuple, optional
        Selection of images to work on.
    uncert : bool, optional
        Whether to return standard deviation alongside average value.
    nprocs : int
        Number of processes to use.

    Returns
    -------
        float
    Averaged brightness.
    """
    ret = cumul_bright_sequence(chunk, select, nprocs)
    if uncert:
        return ret.mean(), ret.std()
    else:
        return ret.mean()


def luminance_sequence(seq, fov, ref, noise, select=None, processes=NPROCS):
    """Computes the luminance of frame sequence `seq`.

    Parameters
    ----------
    seq : Slicerator
        Image sequence, or part of image sequence.
    fov : float
        Camera's field of view (typically in square meters).
    ref : float
        Reference brightness (melt brightness) to normalize.
    noise : float
        Noise (brightness) level. Typically computed by calling
        `average_cbright()` with a suitable selection of the main sequence.
    select : tuple
     Subset selection of images. If not specified, the whole image is selected
    processes : int, optional
        Number of processes to use (default is number of host CPUs).

    Returns
    -------
        ndarray
    Luminance (in same units as `fov`) for each frame in given input sequence.
    """
    return fov * (cumul_bright_sequence(seq, select, processes) - noise) \
        / (ref - noise)


def sigma_luminance(lum, ref, sref, noise, snoise, fov, sfov) -> np.ndarray:
    """Standard deviation of luminance.

    Parameters
    ----------
    lum : float od ndarray
        Luminance
    ref : float
        Reference (melt) brightness.
    sref : float
        Standard deviation of `ref`.
    noise : float
        Background noise brightness (B0).
    snoise : float
        Standard deviation of `noise`.
    fov : float
        Camera's or frame's field of view.
    sfov : float
        Standard deviation of `fov`
    """
    return np.sqrt(
        (lum / (ref - noise)) ** 2 * sref ** 2
        + (-fov / (ref - noise) + lum / (ref - noise)) ** 2 * snoise ** 2
        + (lum / fov) ** 2 * sfov ** 2
    )


def sigma_ldot(ldot, lum, slum, srate, ssrate, lmin=1e-4):
    """Standard deviation of Ldot (time derivative of luminance).

    Parameters
    ----------
    ldot : ndarray or float
        Luminance time derivative.
    lum : ndarray or float
        Luminance.
    slum : ndarray or float
        Standard deviation of luminance.
    srate : float
        sampling rate.
    ssrate : float
        standard devition of sampling rate.
    lmin : float
        Minimum value of luminance. if `lum` is smaller than this value the
         return value will be made invalid (nan).
    """
    if isinstance(ldot, ndarray):
        idx = lum >= lmin
        ret = np.nan * np.empty_like(ldot)
        ret[idx] = np.sqrt(
            (ldot[idx] * slum[idx] / lum[idx]) ** 2
            + (ssrate / srate * ldot[idx]) ** 2
        )
        return ret
    else:
        if lum < lmin:
            return np.nan
        else:
            return np.sqrt((ldot * slum / lum) ** 2
                           + (ldot * ssrate / srate) ** 2)


def dldot(t, lum, threshold=None, invalid=np.nan) -> np.ndarray:
    """Speed associated with luminance. Returns v = dot(L) / (2 sqrt(L)).

    Parameters
    ----------
    t : ndarray
        Time array.
    lum : UnivariateSpline
        Luminance spline. This has to be a spline, since the smoothing should be
        done manually.
    threshold : float
        Noise threshold value. At luminosities below this value `dldot` will
        return the value passed to `invalid`.
    invalid : ndarray
        Number to use to signify an invalid value (NaN)
    """
    lumn = lum(t)
    ret = np.empty_like(lumn)
    if threshold is not None:
        idx = lumn < threshold
        ret[idx] = invalid
        idx = np.logical_not(idx)
        ret[idx] = lum.derivative(n=1)(t[idx]) / (2 * np.sqrt(lumn[idx]))
    else:
        ret = lum.derivative(n=1)(t) / (2 * np.sqrt(lumn))
    return ret


def sigma_dldot(ldot, sldot, lum, slum, lmin: float):
    """Standard deviation of dldot.
    All parameters except `lmin` should be given either as ndarray, or as
    float, mixed parameter sets will return an error.

    Parameters
    ----------
    ldot : float or ndarray
        Time derivative of luminance.
    sldot : float or ndarray
        Standard deviation of `ldot`.
    lum : float or ndarray
        Luminance.
    slum : float or ndarray
        Uncertainty of `lum`.
    lmin : float
        Minimum luminance for which to compute `sigma_dldot`.

    Returns
    -------
        float or ndarray
    1-sigma uncertainty of dldot.
    """
    if isinstance(ldot, ndarray):
        ret = np.nan * np.empty_like(ldot)
        idx = lum > lmin
        ret[idx] = np.sqrt(
            (sldot[idx] / (2 * lum[idx] ** 0.5)) ** 2 +
            (ldot[idx] * slum[idx] / (4 * lum[idx] ** 1.5)) ** 2
        )
    else:
        assert not isinstance(lum, ndarray), \
            "if ldot is given as float, lum must be, too."
        if lum < lmin:
            ret = np.nan
        else:
            ret = np.sqrt(
                (sldot / (2 * lum ** 0.5)) ** 2 +
                (ldot * slum / (4 * lum ** 1.5)) ** 2
            )
    return ret


class FilterProfile:
    """Profile object to store camera specific filter profiles.

    Attributes
    ----------
    cname : str
        A camera identifier.
    srate : float
        Camera's sampling rate.
    freqs : float
        Array of frequencies that should be damped from signal.
    filterwidth : float
        Frequency width of the sink in the filter response around each value
        in `filterfreq`.
    """
    def __init__(self, cname, srate, freqs, filterwidth=1.5):
        """See above.
        """
        self.cname = cname
        self.srate = srate
        self.freqs = freqs
        self.filterwidth = filterwidth


prof_casiof1 = FilterProfile(
    'Casio F1', srate=300.,
    freqs=np.array([30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140])
)


def filter_lum(lum, profile):
    """Filters a luminance (or any other) signal according to the given filter
    profile.

    Parameters
    ----------
    lum : ndarray
        'raw' signal to be filtered.
    profile : FilterProfile
        Filter profile.

    Returns
    -------
        ndarray
    Filtered signal.
    """
    fw = profile.filterwidth
    r = profile.srate
    pars = [
        sig.bessel(
            2, np.array([fr - fw, fr + fw]) / (0.5 * r),
            btype='bandstop', analog=False)
        for fr in profile.freqs
    ]
    filtered = lum.copy()
    for bb, ab in pars:
        filtered = sig.filtfilt(bb, ab, filtered, axis=0)
    return filtered
