`count`
^^^^^^^

.. automodule:: measuremelt.count

.. autofunction:: measuremelt.count.count_parallel

.. autofunction:: measuremelt.count.average_count

.. autofunction:: measuremelt.count.area_sequence
