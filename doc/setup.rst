Setup Notes
===========

Software Dependencies
---------------------

1. The code depends on the command line of the :code:`ffmpeg` utility. In order to
   convert the videos to image sequences, it needs to be available on the 
   command line system-wide.
  - On *Linux* this should be available as a standard package.
  - On *macOS* `this guide: <https://superuser.com/questions/624561/>`_
    shows
    several ways to set it up. The second choice seems to be the easiest.
  - On *Windows*
    `this is a nice setup guide on StackExchange <https://video.stackexchange.com/questions/20495/>`_.
2. The code is written in Python, and depends on a number of scientific
   packages. If a Python distribution with the :code:`pip` tool is available this
   should work. Otherwise, the
   `Anaconda distribution <https://www.anaconda.com/distribution/>`_
   is recommended for use. If using Anaconda make sure to install the Python 3
   version. The **code will not run under Python 2**. Minimum Python version at
   the moment in **3.8**. It may be useful to run
   things in a virtual environment; but this is not strictly necessary.
3. The package should install without problems using :code:`pip`. Since this is a
   somewhat specialized use case scenario it is probably best to install this
   in a virtual environment. From the :code:`measuremelt` folder, the command

   .. code-block::

      pip install --upgrade -e .

   will install the package and the necessary dependencies.


Hard dependencies are:

- numpy
- pathlib
- pillow
- pims


Optional dependencies, and necessary to run the example notebook:

- matplotlib
- jupyter
  
When using :code:`conda` the :code:`pims` package in the standard repo may be
a little old. The conda-forge repo likely has the up-to-date version

.. code-block:: shell

  conda install -c conda-forge pims

Setup
-----

Download or clone this package from GitHub using the above green button, and
unzip the contents into a location with write access. Then start a local jupyter
lab or notebook server either from the menu or from the command line.
From the notebook file browser open the `example.ipynb` notebook and enjoy.

If `git` is available the package can be cloned directly from GitLab:

.. code-block:: bash

    git clone https://gitlab.com/isonder/measuremelt.git


This will create a local copy of the upstream repository in a local folder
named :code:`measuremelt`.


Quickstart
----------

All of the above can be skipped if `pip`, `virtualenvwrapper`, and `git` are
available. Then the following commands will clone the package in the current
directory, create a virtual environment called `lum`, and install the necessary
dependencies:

.. code-block:: bash

    git clone https://gitlab.com/isonder/measuremelt.git
    mkvirtualenv -p /usr/bin/python3 lum
    toggleglobalsitepackages
    cd measuremelt
    pip install --upgrade -e .


Build Documentation
-------------------

Install the optional dependencies for documentation building:

- sphinx
- sphinx-rtd-theme
- numpydoc

Then run

.. code-block:: bash

    sphinx-build -b html srcdir targetdir

where :code:`srcdir` is the :code:`doc` folder of this package, and
:code:`targetdir` is whereever the html output should be stored.
