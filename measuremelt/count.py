import numpy as np
from numpy.typing import NDArray
from typing import Union, Callable

from . import NPROCS, set_processing

ProcOrThread, Queue = set_processing()
tuple_or_num = Union[tuple, int, float]
tuple_or_int = Union[tuple, int]
int_or_float = Union[int, float]


def threshold(arr: NDArray, thr: tuple_or_num, ret_idx: bool = False) -> tuple_or_int:
    """Count elements of arr smaller than thresholds specified in `thr`.

    Parameters
    ----------
    arr : 2D or 3D ndarray
        A grayscale or color image; or a subset of one.
    thr : tuple or a number
        Threshold parameter(s). If `arr` is a 2D image this should be a number.
        If `arr` is a 3D (color) array the threshold values are assumed to
        correspond to the third axis of `arr`.
    ret_idx : bool, optional
        Whether to return index where condition is met.

    Returns
    -------

    """
    shape = arr.shape[:2]
    thr = thr if isinstance(thr, tuple) else (thr,)
    idx = np.ones(shape, dtype=bool)
    for comp in thr:
        idx = np.logical_and(idx, arr >= comp)
    n = np.count_nonzero(idx)
    return (n, idx) if ret_idx else n


def thr_lum_to_ch(
        arr: NDArray, thr_lum: int_or_float, lum_to_ch: float, ch: int,
        ret_idx: bool = False
) -> tuple_or_num:
    """

    Parameters
    ----------
    arr : 3D ndarray
    thr_lum : int or float
    lum_to_ch : float
    ch : int
    ret_idx : bool

    Returns
    -------

    """
    if len(arr.shape) < 3:
        raise ValueError(
            "Cannot count lum_to_ch threshold from a gray scale image.\n"
            f"{arr.shape=}"
        )
    if ch > 2:
        raise ValueError(
            f"ch must be a valid color channel integer (0, 1, 2), got {ch=}."
        )
    # lum = 0.2125 red + 0.7154 green + 0.0721 blue
    agray = .2125 * arr[:, :, 0] + .7154 * arr[:, :, 1] + .0721 * arr[:, :, 2]
    test = arr[:, :, ch].copy()
    test[np.where(test == 0)] = 1.0e-3
    idx = np.logical_and(agray >= thr_lum, agray / test <= lum_to_ch)
    n = np.count_nonzero(idx)
    return (n, idx) if ret_idx else n


def thr_lum_to_blue(
        arr: NDArray, thr_lum: int_or_float, lum_to_b: float,
        ret_idx: bool = False
) -> tuple_or_num:
    """Count number of pixels which have a larger luminance than a threshold
    and which have a larger luminance-to-blue ratio as given parameter.

    Parameters
    ----------
    arr : 3D ndarray
        Color image to analyze.
    thr_lum : int or float
        Luminance threshold
    lum_to_b : float
        Luminance to blue ratio.
    ret_idx : bool
        Whether to return the index array where the condition is met.

    Returns
    -------
    Count and optionally index array of pixels for which condition is true.
    """
    return thr_lum_to_ch(arr, thr_lum, lum_to_b, 2, ret_idx)


def count_from_chunk(
        chunk: NDArray, select: tuple, start: int, end: int,
        que: Queue, count_func: Callable, func_args: tuple
):
    """Apply the `count_func` function to each element of `chunk`.

    Parameters
    ----------
    chunk : Slicerator
    select : tuple
    start : int
    end : int
    que : mp.Queue
    count_func : function
    func_args : tuple
    """
    ret = np.empty(len(chunk), dtype=int)
    for i, frame in enumerate(chunk):
        if select is None:
            start0, start1 = 0, 0
            end0, end1 = frame.shape
        else:
            ((start0, end0), (start1, end1)) = select
        ret[i] = count_func(frame[start0:end0, start1:end1], *func_args)
    que.put((ret, start, end))


def count_parallel(
        seq: NDArray, select: tuple = None,
        count_func: Callable = None, func_args: tuple = None,
        processes: int = NPROCS
) -> NDArray:
    """

    Parameters
    ----------
    seq : NDArray or any of its subclasses
        Sequence of frames to analyze.
    select : tuple
    count_func : function
    func_args : tuple
    processes : int

    Returns
    -------

    """
    itms_per_proc = len(seq) // processes
    que = Queue()
    start, end, procs, idx = 0, -1, [], []
    for k in range(processes):
        end = start + itms_per_proc
        args = (seq[start:end], select, start, end, que, count_func, func_args)
        p = ProcOrThread(target=count_from_chunk, args=args)
        procs.append(p)
        start = end
        p.start()
    args = (seq[start:], select, start, len(seq), que, count_func, func_args)
    p = ProcOrThread(target=count_from_chunk, args=args)
    procs.append(p)
    p.start()
    ret = np.empty(len(seq), dtype=int)
    for _ in range(len(procs)):
        chunk, start, end = que.get()
        try:
            ret[start:end] = chunk
        except ValueError:
            print(f"{start=}, {end=},\n{chunk=}")
            raise
    for p in procs:
        p.join()
    return ret


def average_count(
        chunk: NDArray, select: tuple = None, uncert: bool = False,
        count_func: Callable = threshold, func_args: tuple = None,
        processes: int = NPROCS
):
    """Calculate an average value over `chunk` of whatever `count_func()`
    returns.

    Parameters
    ----------
    chunk : NDArray
    select : tuple, optional
        Subset selection that is applied to each frame.
    uncert : bool
        Whether to return the standard deviation associated with the mean value.
    count_func : function
        User supplied counting function
    func_args : tuple
        Arguments for `count_func`
    processes : int
        Number of processes or threads to use. Default is the return value of
        multiprocessing.cpucount().

    Returns
    -------

    """
    ret = count_parallel(chunk, select, count_func, func_args, processes)
    if uncert:
        return ret.mean(), ret.std()
    else:
        return ret.mean()


# noinspection PyUnresolvedReferences
def area_sequence(
        seq: NDArray, fov: float, noise: float, select: tuple = None,
        count_func: Callable = threshold, func_args: tuple = None,
        processes: int = NPROCS
):
    """Calculate the total area for which the return value of `count_func` is
    true, for all frames in `seq`.

    Parameters
    ----------
    seq : Slicerator
        The (part of an) image sequence.
    fov : float
        Field of view: area (e.e. square meters!) that is visible in one whole
        frame.
    noise : float
        Noise amount present in video. This can be calculated with
        `average_count()`.
    select : tuple
        Subset specifier which is applied to each frame. By default, the whole
        image is used.
    count_func : function
        Counting function that determines which pixels contain melt.
    func_args : tuple
        Arguments for `count_func`.
    processes : int
        Number of processes to use. By default, this is whatever `cpu_count()`
        returns.

    Returns
    -------

    """
    if select is None:
        try:
            shp = seq.frame_shape[:2]
        except AttributeError:
            shp = seq.shape[1:3]
        mx = shp[0] * shp[1]
    else:
        mx = (select[0][1] - select[0][0]) * (select[1][1] - select[1][0])
    cnt = count_parallel(seq, select, count_func, func_args, processes)
    return fov * (cnt - noise) / (mx - noise)
